<?php

namespace T3easy\Impress\Tests;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \T3easy\Impress\Domain\Model\Presentation.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Impress
 *
 * @author Jan Kiesewetter <janYYYY@t3easy.de>
 */
class PresentationTest extends \TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase {
	/**
	 * @var \T3easy\Impress\Domain\Model\Presentation
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new \T3easy\Impress\Domain\Model\Presentation();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}
	
	/**
	 * @test
	 */
	public function getTransitionDurationReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getTransitionDuration()
		);
	}

	/**
	 * @test
	 */
	public function setTransitionDurationForIntegerSetsTransitionDuration() { 
		$this->fixture->setTransitionDuration(12);

		$this->assertSame(
			12,
			$this->fixture->getTransitionDuration()
		);
	}
	
	/**
	 * @test
	 */
	public function getPerspectiveReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getPerspective()
		);
	}

	/**
	 * @test
	 */
	public function setPerspectiveForIntegerSetsPerspective() { 
		$this->fixture->setPerspective(12);

		$this->assertSame(
			12,
			$this->fixture->getPerspective()
		);
	}
	
	/**
	 * @test
	 */
	public function getSlidesReturnsInitialValueForSlide() { 
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getSlides()
		);
	}

	/**
	 * @test
	 */
	public function setSlidesForObjectStorageContainingSlideSetsSlides() { 
		$slide = new \T3easy\Impress\Domain\Model\Slide();
		$objectStorageHoldingExactlyOneSlides = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$objectStorageHoldingExactlyOneSlides->attach($slide);
		$this->fixture->setSlides($objectStorageHoldingExactlyOneSlides);

		$this->assertSame(
			$objectStorageHoldingExactlyOneSlides,
			$this->fixture->getSlides()
		);
	}
	
	/**
	 * @test
	 */
	public function addSlideToObjectStorageHoldingSlides() {
		$slide = new \T3easy\Impress\Domain\Model\Slide();
		$objectStorageHoldingExactlyOneSlide = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$objectStorageHoldingExactlyOneSlide->attach($slide);
		$this->fixture->addSlide($slide);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneSlide,
			$this->fixture->getSlides()
		);
	}

	/**
	 * @test
	 */
	public function removeSlideFromObjectStorageHoldingSlides() {
		$slide = new \T3easy\Impress\Domain\Model\Slide();
		$localObjectStorage = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$localObjectStorage->attach($slide);
		$localObjectStorage->detach($slide);
		$this->fixture->addSlide($slide);
		$this->fixture->removeSlide($slide);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getSlides()
		);
	}
	
}
?>