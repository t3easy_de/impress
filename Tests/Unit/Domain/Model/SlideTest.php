<?php

namespace T3easy\Impress\Tests;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \T3easy\Impress\Domain\Model\Slide.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Impress
 *
 * @author Jan Kiesewetter <janYYYY@t3easy.de>
 */
class SlideTest extends \TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase {
	/**
	 * @var \T3easy\Impress\Domain\Model\Slide
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new \T3easy\Impress\Domain\Model\Slide();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getIdReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setIdForStringSetsId() {
		$this->fixture->setId('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getId()
		);
	}

	/**
	 * @test
	 */
	public function getContentReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setContentForStringSetsContent() {
		$this->fixture->setContent('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getContent()
		);
	}

	/**
	 * @test
	 */
	public function getAdditionalClassReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getAdditionalClass()
		);
	}

	/**
	 * @test
	 */
	public function setAdditionalClassForIntegerSetsAdditionalClass() {
		$this->fixture->setAdditionalClass(12);

		$this->assertSame(
			12,
			$this->fixture->getAdditionalClass()
		);
	}

	/**
	 * @test
	 */
	public function getXReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getX()
		);
	}

	/**
	 * @test
	 */
	public function setXForIntegerSetsX() {
		$this->fixture->setX(12);

		$this->assertSame(
			12,
			$this->fixture->getX()
		);
	}

	/**
	 * @test
	 */
	public function getYReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getY()
		);
	}

	/**
	 * @test
	 */
	public function setYForIntegerSetsY() {
		$this->fixture->setY(12);

		$this->assertSame(
			12,
			$this->fixture->getY()
		);
	}

	/**
	 * @test
	 */
	public function getZReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getZ()
		);
	}

	/**
	 * @test
	 */
	public function setZForIntegerSetsZ() {
		$this->fixture->setZ(12);

		$this->assertSame(
			12,
			$this->fixture->getZ()
		);
	}

	/**
	 * @test
	 */
	public function getScaleReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getScale()
		);
	}

	/**
	 * @test
	 */
	public function setScaleForIntegerSetsScale() {
		$this->fixture->setScale(12);

		$this->assertSame(
			12,
			$this->fixture->getScale()
		);
	}

	/**
	 * @test
	 */
	public function getRotateReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getRotate()
		);
	}

	/**
	 * @test
	 */
	public function setRotateForIntegerSetsRotate() {
		$this->fixture->setRotate(12);

		$this->assertSame(
			12,
			$this->fixture->getRotate()
		);
	}

	/**
	 * @test
	 */
	public function getRotateXReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getRotateX()
		);
	}

	/**
	 * @test
	 */
	public function setRotateXForIntegerSetsRotateX() {
		$this->fixture->setRotateX(12);

		$this->assertSame(
			12,
			$this->fixture->getRotateX()
		);
	}

	/**
	 * @test
	 */
	public function getRotateYReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->fixture->getRotateY()
		);
	}

	/**
	 * @test
	 */
	public function setRotateYForIntegerSetsRotateY() {
		$this->fixture->setRotateY(12);

		$this->assertSame(
			12,
			$this->fixture->getRotateY()
		);
	}

}
?>