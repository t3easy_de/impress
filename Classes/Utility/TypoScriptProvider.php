<?php
namespace T3easy\Impress\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class TypoScriptProvider implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * Object manager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * Configuration manager
	 *
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * construct
	 */
	public function __construct() {
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
	}

	/**
	 * Get the settings
	 * @return array
	 */
	protected function getSettings() {
		$frameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
		return $frameworkConfiguration['plugin.']['tx_impress.']['settings.'];
	}

	/**
	 * Get additional classes
	 *
	 * @param array $config
	 * @return array
	 */
	public function getAdditionalClasses(array $config) {
		$settings = $this->getSettings();
		$additionalClassesItems[] = array(
			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
				'LLL:EXT:impress/Resources/Private/Language/locallang_be.xlf:tx_impress_domain_model_slide.additional_class.none',
				'Impress'),
			''
		);
		$additionalClasses = $settings['additionalClasses.'];
		foreach ($additionalClasses as $name => $label) {
			$additionalClassesItems[] = array($label, $name);
		}
		$config['items'] = array_merge($config['items'], $additionalClassesItems);
		return $config;
	}

	/**
	 * Get additional classes
	 *
	 * @param array $config
	 * @return array
	 */
	public function getIds(array $config) {
		$settings = $this->getSettings();
		$idsItems[] = array(
			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
				'LLL:EXT:impress/Resources/Private/Language/locallang_be.xlf:tx_impress_domain_model_slide.id.none',
				'Impress'),
			''
		);
		$ids = $settings['ids.'];
		foreach ($ids as $name => $label) {
			$idsItems[] = array($label, $name);
		}
		$config['items'] = array_merge($config['items'], $idsItems);
		return $config;
	}

	/**
	 * Get degrees range
	 *
	 * @return array
	 */
	public function getDegreesRange() {
		$settings = $this->getSettings();
		$degreesConfig['range']['lower'] = $settings['degreesConfig.']['range.']['lower'];
		$degreesConfig['range']['upper'] = $settings['degreesConfig.']['range.']['upper'];
		$degreesConfig['step'] = $settings['degreesConfig.']['step'];
		$degreesConfig['width'] = $settings['degreesConfig.']['width'];
		return $degreesConfig;
	}

}

?>