<?php
namespace T3easy\Impress\ViewHelpers\Link;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class TouchIconViewHelper extends AbstractLinkViewHelper {

	/**
	 * Arguments initialization
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerTagAttribute('sizes', 'string', 'Specifies the size of the linked resource, e.g. 16x16');
	}

	/**
	 * Add the touch Icon to the header
	 *
	 * @param bool $precomposed
	 * @return void
	 */
	public function render($precomposed = FALSE) {
		if ($precomposed) {
			$this->tag->addAttribute('rel', 'apple-touch-icon-precomposed');
		} else {
			$this->tag->addAttribute('rel', 'apple-touch-icon');
		}
		if (!$this->viewHelperVariableContainer->exists('T3easy\Impress\ViewHelpers\Link\TouchIconViewHelper', 'isSet')){
			$GLOBALS['TSFE']->getPageRenderer()->addMetaTag($this->tag->render());
			$this->viewHelperVariableContainer->add('T3easy\Impress\ViewHelpers\Link\TouchIconViewHelper', 'isSet', TRUE);
		}
	}

}

?>