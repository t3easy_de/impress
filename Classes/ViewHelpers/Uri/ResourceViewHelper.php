<?php
namespace T3easy\Impress\ViewHelpers\Uri;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */
/**
 * A view helper for creating URIs to resources.
 *
 * = Examples =
 *
 * <code title="Defaults">
 * {namespace i=T3easy\Impress\ViewHelpers}
 * <link href="{i:uri.resource(path:'EXT:my_extension/Resources/Public/favicon.png')}" rel="shortcut icon" />
 * </code>
 * <output>
 * <link href="typo3conf/ext/my_extension/Resources/Public/favicon.png" rel="stylesheet" />
 * (depending on current package)
 * </output>
 */
class ResourceViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render the URI to the resource. The filename is used from child content.
	 *
	 * @param string $path The path and filename of the resource (relative to Public resource directory of the extension).
	 * @param boolean $absolute If set, an absolute URI is rendered
	 * @return string The URI to the resource
	 */
	public function render($path, $absolute = FALSE) {
		$uri = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($path);
		$uri = substr($uri, strlen(PATH_site));
		if (TYPO3_MODE === 'BE' && $absolute === FALSE && $uri !== FALSE) {
			$uri = '../' . $uri;
		}
		if ($absolute === TRUE) {
			$uri = $this->controllerContext->getRequest()->getBaseURI() . $uri;
		}
		return $uri;
	}
}

?>