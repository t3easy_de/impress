<?php
namespace T3easy\Impress\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package impress
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PresentationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * presentationRepository
	 *
	 * @var \T3easy\Impress\Domain\Repository\PresentationRepository
	 * @inject
	 */
	protected $presentationRepository;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$presentations = $this->presentationRepository->findAll();
		$this->view->assign('presentations', $presentations);
	}

	/**
	 * action show
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $presentation
	 * @return void
	 */
	public function showAction(\T3easy\Impress\Domain\Model\Presentation $presentation) {
		$GLOBALS['TSFE']->page['title'] = $presentation->getTitle();
		$GLOBALS['TSFE']->indexedDocTitle = $presentation->getTitle();
		$this->view->assign('presentation', $presentation);
	}

	/**
	 * action new
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $newPresentation
	 * @dontvalidate $newPresentation
	 * @return void
	 */
	public function newAction(\T3easy\Impress\Domain\Model\Presentation $newPresentation = NULL) {
		$this->view->assign('newPresentation', $newPresentation);
	}

	/**
	 * action create
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $newPresentation
	 * @return void
	 */
	public function createAction(\T3easy\Impress\Domain\Model\Presentation $newPresentation) {
		$this->presentationRepository->add($newPresentation);
		$this->flashMessageContainer->add('Your new Presentation was created.');
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $presentation
	 * @return void
	 */
	public function editAction(\T3easy\Impress\Domain\Model\Presentation $presentation) {
		$this->view->assign('presentation', $presentation);
	}

	/**
	 * action update
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $presentation
	 * @return void
	 */
	public function updateAction(\T3easy\Impress\Domain\Model\Presentation $presentation) {
		$this->presentationRepository->update($presentation);
		$this->flashMessageContainer->add('Your Presentation was updated.');
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \T3easy\Impress\Domain\Model\Presentation $presentation
	 * @return void
	 */
	public function deleteAction(\T3easy\Impress\Domain\Model\Presentation $presentation) {
		$this->presentationRepository->remove($presentation);
		$this->flashMessageContainer->add('Your Presentation was removed.');
		$this->redirect('list');
	}

}
?>