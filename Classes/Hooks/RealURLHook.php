<?php
namespace T3easy\Impress\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class RealURLHook {

	/**
	 * Add the RealURL config
	 *
	 * @param $params
	 * @param $pObj
	 * @return array
	 */
	public function addImpressConfig($params, &$pObj) {
		return array_merge_recursive(
			$params['config'],
			array(
				/*
				'fileName' => array (
					'index' => array(
						'impress.html' => array(
							'keyValues' => array (
								'type' => 1362576330
							)
						)
					)
				),
				*/
				'postVarSets' => array(
					'_DEFAULT' => array(
						'presentation' => array(
							array(
								'GETvar' => 'tx_impress_main[presentation]',
								'lookUpTable' => array(
									'table' => 'tx_impress_domain_model_presentation',
									'id_field' => 'uid',
									'alias_field' => 'title',
									'addWhereClause' => ' AND NOT deleted',
									'useUniqueCache' => 1,
									'useUniqueCache_conf' => array(
										'strtolower' => 1,
										'spaceCharacter' => '-',
									)
								)
							)
						),
						'impress' => array(
							'type' => 'single',
							'keyValues' => array(
								'type' => 1362576330
							)
						)
					)
				)
			)
		);
	}

}
