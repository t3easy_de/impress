<?php
namespace T3easy\Impress\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package impress
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Slide extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * id
	 *
	 * @var \string
	 */
	protected $id;

	/**
	 * content
	 *
	 * @var \string
	 */
	protected $content;

	/**
	 * additionalClass
	 *
	 * @var \string
	 */
	protected $additionalClass;

	/**
	 * x
	 *
	 * @var \integer
	 */
	protected $x;

	/**
	 * y
	 *
	 * @var \integer
	 */
	protected $y;

	/**
	 * z
	 *
	 * @var \integer
	 */
	protected $z;

	/**
	 * scale
	 *
	 * @var \float
	 */
	protected $scale = 1.00;

	/**
	 * rotate
	 *
	 * @var \integer
	 */
	protected $rotate;

	/**
	 * rotateX
	 *
	 * @var \integer
	 */
	protected $rotateX;

	/**
	 * rotateY
	 *
	 * @var \integer
	 */
	protected $rotateY;

	/**
	 * sorting
	 *
	 * @var \integer
	 */
	protected $sorting;

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the id
	 *
	 * @return \string $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Sets the id
	 *
	 * @param \string $id
	 * @return void
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * Returns the content
	 *
	 * @return \string $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Sets the content
	 *
	 * @param \string $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Returns the additionalClass
	 *
	 * @return \string $additionalClass
	 */
	public function getAdditionalClass() {
		return $this->additionalClass;
	}

	/**
	 * Sets the additionalClass
	 *
	 * @param \string $additionalClass
	 * @return void
	 */
	public function setAdditionalClass($additionalClass) {
		$this->additionalClass = $additionalClass;
	}

	/**
	 * Returns the x
	 *
	 * @return \integer $x
	 */
	public function getX() {
		return $this->x;
	}

	/**
	 * Sets the x
	 *
	 * @param \integer $x
	 * @return void
	 */
	public function setX($x) {
		$this->x = $x;
	}

	/**
	 * Returns the y
	 *
	 * @return \integer $y
	 */
	public function getY() {
		return $this->y;
	}

	/**
	 * Sets the y
	 *
	 * @param \integer $y
	 * @return void
	 */
	public function setY($y) {
		$this->y = $y;
	}

	/**
	 * Returns the z
	 *
	 * @return \integer $z
	 */
	public function getZ() {
		return $this->z;
	}

	/**
	 * Sets the z
	 *
	 * @param \integer $z
	 * @return void
	 */
	public function setZ($z) {
		$this->z = $z;
	}

	/**
	 * Returns the scale
	 *
	 * @return \float $scale
	 */
	public function getScale() {
		return $this->scale;
	}

	/**
	 * Sets the scale
	 *
	 * @param \float $scale
	 * @return void
	 */
	public function setScale($scale) {
		$this->scale = $scale;
	}

	/**
	 * Returns the rotate
	 *
	 * @return \integer $rotate
	 */
	public function getRotate() {
		return $this->rotate;
	}

	/**
	 * Sets the rotate
	 *
	 * @param \integer $rotate
	 * @return void
	 */
	public function setRotate($rotate) {
		$this->rotate = $rotate;
	}

	/**
	 * Returns the rotateX
	 *
	 * @return \integer $rotateX
	 */
	public function getRotateX() {
		return $this->rotateX;
	}

	/**
	 * Sets the rotateX
	 *
	 * @param \integer $rotateX
	 * @return void
	 */
	public function setRotateX($rotateX) {
		$this->rotateX = $rotateX;
	}

	/**
	 * Returns the rotateY
	 *
	 * @return \integer $rotateY
	 */
	public function getRotateY() {
		return $this->rotateY;
	}

	/**
	 * Sets the rotateY
	 *
	 * @param \integer $rotateY
	 * @return void
	 */
	public function setRotateY($rotateY) {
		$this->rotateY = $rotateY;
	}

	/**
	 * Returns sorting
	 *
	 * @return \integer
	 */
	public function getSorting() {
		return $this->sorting;
	}

	/**
	 * Sets sorting
	 *
	 * @param \integer $sorting
	 */
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}

}
?>