<?php
namespace T3easy\Impress\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package impress
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Presentation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var \string
	 * @validate NotEmpty
	 */
	protected $title;

	/**
	 * author
	 *
	 * @var \string
	 */
	protected $author;

	/**
	 * transitionDuration
	 *
	 * @var \integer
	 */
	protected $transitionDuration;

	/**
	 * perspective
	 *
	 * @var \integer
	 */
	protected $perspective;

	/**
	 * displayHint
	 *
	 * @var boolean
	 */
	protected $displayHint = TRUE;

	/**
	 * Shortcut icon for this presentation
	 *
	 * @var \string
	 */
	protected $shortcutIcon;

	/**
	 * Touch icon for this presentation
	 *
	 * @var \string
	 */
	protected $touchIcon;

	/**
	 * slides
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3easy\Impress\Domain\Model\Slide>
	 */
	protected $slides;

	/**
	 * __construct
	 *
	 * @return Presentation
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->slides = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the author
	 *
	 * @return \string $author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * Sets the author
	 *
	 * @param string $author
	 * @return void
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * Returns the transitionDuration
	 *
	 * @return \integer $transitionDuration
	 */
	public function getTransitionDuration() {
		return $this->transitionDuration;
	}

	/**
	 * Sets the transitionDuration
	 *
	 * @param \integer $transitionDuration
	 * @return void
	 */
	public function setTransitionDuration($transitionDuration) {
		$this->transitionDuration = $transitionDuration;
	}

	/**
	 * Returns the perspective
	 *
	 * @return \integer $perspective
	 */
	public function getPerspective() {
		return $this->perspective;
	}

	/**
	 * Sets the perspective
	 *
	 * @param \integer $perspective
	 * @return void
	 */
	public function setPerspective($perspective) {
		$this->perspective = $perspective;
	}

	/**
	 * Returns the displayHint
	 *
	 * @return boolean $displayHint
	 */
	public function getDisplayHint() {
		return $this->displayHint;
	}

	/**
	 * Sets the displayHint
	 *
	 * @param boolean $aaa
	 * @return void
	 */
	public function setDisplayHint($aaa) {
		$this->displayHint = $aaa;
	}

	/**
	 * Returns the shortcutIcon
	 *
	 * @return \string $shortcutIcon
	 */
	public function getShortcutIcon() {
		return $this->shortcutIcon;
	}

	/**
	 * Sets the shortcutIcon
	 *
	 * @param \string $shortcutIcon
	 * @return void
	 */
	public function setShortcutIcon($shortcutIcon) {
		$this->shortcutIcon = $shortcutIcon;
	}

	/**
	 * Returns the touchIcon
	 *
	 * @return \string $touchIcon
	 */
	public function getTouchIcon() {
		return $this->touchIcon;
	}

	/**
	 * Sets the touchIcon
	 *
	 * @param \string $touchIcon
	 * @return void
	 */
	public function setTouchIcon($touchIcon) {
		$this->touchIcon = $touchIcon;
	}

	/**
	 * Adds a Slide
	 *
	 * @param \T3easy\Impress\Domain\Model\Slide $slide
	 * @return void
	 */
	public function addSlide(\T3easy\Impress\Domain\Model\Slide $slide) {
		$this->slides->attach($slide);
	}

	/**
	 * Removes a Slide
	 *
	 * @param \T3easy\Impress\Domain\Model\Slide $slideToRemove The Slide to be removed
	 * @return void
	 */
	public function removeSlide(\T3easy\Impress\Domain\Model\Slide $slideToRemove) {
		$this->slides->detach($slideToRemove);
	}

	/**
	 * Returns the slides
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3easy\Impress\Domain\Model\Slide> $slides
	 */
	public function getSlides() {
		return $this->slides;
	}

	/**
	 * Sets the slides
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3easy\Impress\Domain\Model\Slide> $slides
	 * @return void
	 */
	public function setSlides(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $slides) {
		$this->slides = $slides;
	}

}
?>