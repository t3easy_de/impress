<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'T3easy.' . $_EXTKEY,
	'Main',
	array(
		'Presentation' => 'list, show'
	),
	// non-cacheable actions
	array(
		'Presentation' => ''
	)
);

// RealURL auto-configuration
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['impress'] = 'T3easy\Impress\Hooks\RealURLHook->addImpressConfig';
}
?>