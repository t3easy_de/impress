<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_impress_domain_model_presentation'] = array(
	'ctrl' => $TCA['tx_impress_domain_model_presentation']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, author, transition_duration, perspective, display_hint, shortcut_icon, touch_icon, slides',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden, title, author, slides,
			--div--;LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.settings,
				transition_duration, perspective, display_hint, shortcut_icon, touch_icon,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_impress_domain_model_presentation',
				'foreign_table_where' => 'AND tx_impress_domain_model_presentation.pid=###CURRENT_PID### AND tx_impress_domain_model_presentation.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'author' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.author',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'transition_duration' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.transition_duration',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'perspective' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.perspective',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'display_hint' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.display_hint',
			'config' => array(
				'type' => 'check',
				'default' => 1,
			),
		),
		'shortcut_icon' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.shortcut_icon',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/tx_impress',
				'show_thumbs' => 1,
				'size' => 1,
				'allowed' => 'ico,png',
				'disallowed' => '',
				'minitems' => 0,
				'maxitems' => 1
			),
		),
		'touch_icon' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.touch_icon',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/tx_impress',
				'show_thumbs' => 1,
				'size' => 1,
				'allowed' => 'png',
				'disallowed' => '',
				'minitems' => 0,
				'maxitems' => 1
			),
		),
		'slides' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_presentation.slides',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_impress_domain_model_slide',
				'foreign_field' => 'presentation',
				'foreign_sortby' => 'sorting',
				'maxitems'      => 9999,
				'appearance' => array(
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1,
					'useSortable' => 1
				),
			),
		),
	),
);

?>