<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/** @var $typoScriptProvider T3easy\Impress\Utility\TypoScriptProvider */
$typoScriptProvider = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('T3easy\\Impress\\Utility\\TypoScriptProvider');
$degreesConfigFromTS = $typoScriptProvider->getDegreesRange();
$degreesConfig = array(
	'type' => 'input',
	'size' => 4,
	'eval' => 'int',
	'range' => $degreesConfigFromTS['range'],
	'wizards' => array(
		'degrees' => array(
			'type' => 'slider',
			'step' => $degreesConfigFromTS['step'],
			'width' => $degreesConfigFromTS['width']
		)
	)
);

$TCA['tx_impress_domain_model_slide'] = array(
	'ctrl' => $TCA['tx_impress_domain_model_slide']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, content, id, additional_class, x, y, z, scale, rotate, rotate_x, rotate_y',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden, title, content,
		--div--;LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.settings,
			--palette--;LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.style;style,
			--palette--;LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.position;position, scale,
			--palette--;LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.rotation;rotation,
		--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'style' => array(
			'showitem' => 'id, additional_class',
			'canNotCollapse' => 1,
		),
		'position' => array(
			'showitem' => 'x, y, z',
			'canNotCollapse' => 1,
		),
		'rotation' => array(
			'showitem' => 'rotate, --linebreak--, rotate_x, --linebreak--, rotate_y',
			'canNotCollapse' => 1,
		),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_impress_domain_model_slide',
				'foreign_table_where' => 'AND tx_impress_domain_model_slide.pid=###CURRENT_PID### AND tx_impress_domain_model_slide.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'content' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.content',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords' => 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
			'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts_css|imgpath=uploads/tx_impress/rte/]',
		),
		'id' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.id',
			'config' => array(
				'type' => 'select',
				'items' => array(),
				'itemsProcFunc' => 'T3easy\\Impress\\Utility\\TypoScriptProvider->getIds',
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'additional_class' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.additional_class',
			'config' => array(
				'type' => 'select',
				'items' => array(),
				'itemsProcFunc' => 'T3easy\\Impress\\Utility\\TypoScriptProvider->getAdditionalClasses',
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'x' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.x',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'y' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.y',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'z' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.z',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		),
		'scale' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.scale',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'double2',
				'default' => 1.00,
			),
		),
		'rotate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.rotate',
			'config' => $degreesConfig,
		),
		'rotate_x' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.rotate_x',
			'config' => $degreesConfig,
		),
		'rotate_y' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:impress/Resources/Private/Language/locallang_db.xlf:tx_impress_domain_model_slide.rotate_y',
			'config' => $degreesConfig,
		),
		'presentation' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sorting' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

?>