plugin.tx_impress {
	view {
		# cat=plugin.tx_impress/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:impress/Resources/Private/Templates/
		# cat=plugin.tx_impress/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:impress/Resources/Private/Partials/
		# cat=plugin.tx_impress/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:impress/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_impress//a; type=string; label=Default storage PID
		storagePid =
	}
	settings {
		# cat=plugin.tx_impress/settings; type=int+; label=Type number of presentation
		showTypeNum = 1362576330
		degreesConfig {
			range {
				# cat=plugin.tx_impress/settings; type=int; label=Lower value of degrees range
				lower = 0
				# cat=plugin.tx_impress/settings; type=int; label=Upper value of degrees range
				upper = 3600
			}
			step = 5
			width = 500
		}
		defaultShortcutIcon = EXT:impress/Resources/Public/impress.js/favicon.png
		defaultTouchIcon = EXT:impress/Resources/Public/impress.js/apple-touch-icon.png
	}
}